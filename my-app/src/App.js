import React from 'react';
import ReactDOM from 'react-dom';
import { Container, Row, Col } from 'reactstrap';
import FlipMove from 'react-flip-move';
import logo from './logo.svg';
import './App.css';

// let arr = ['eefdf', 'sdfdsgfg', 'fdfd', 'dgfgsdf'];

class Example extends React.Component {
  render() {
    return (
      <div>
        <b>{
          this.props.text
        } </b>
        This HTML file is a template.
If you open it directly in the browser, you will see an empty page.

You can add webfonts, meta tags, or analytics to this file.
The build step will place the bundled scripts into the tag.

To begin the development, run `npm start` or `yarn start`.
To create a production bundle, use `npm run build` or `yarn build`.
{
          this.props.text
        }
      </div>
    )
  }
}


class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      arr: ['eefdf', 'sdfdsgfg', 'fdfd', 'dgfgsdf'],
      count: 0,
    }
  }

  render() {
    return (
      <React.Fragment>
        <button onClick={() => {
          // this.setState(() => ({ arr: ['sdfdfdffffffffffff', '11111111111', 'sdsdsw22222'] }))
          if (this.state.count % 2) {
            this.setState(() => ({ arr: [], count: this.state.count + 1 }))
          } else {

            this.setState(() => ({ arr: ['eefdf', 'sdfdsgfg', 'fdfd', 'dgfgsdf', 'sdfdfdffffffffffff', '11111111111', 'sdsdsw22222'], count: this.state.count + 1 }))
          }
        }

        }
        >
          Click me
      </button>
        <FlipMove duration={5000} leaveAnimation={customLeaveAnimation}>
          {
            this.state.arr.map((r, index) => (
              <StateFull key={`index_row_statefull_${index}`}>
                <Row key={`index_row_${index}`}>
                  <Col>
                    <div>
                      first test test test
                  </div>
                  </Col>
                  <Col>
                    <Example text={r} key={`index_${index}`} />
                  </Col>
                  <Col>
                    second test test test
            </Col>
                </Row>
              </StateFull>
            )
            )
          }
        </FlipMove>
      </React.Fragment >
    )
  }

}

export default App;

class StateFull extends React.Component {
  render() {
    return (this.props.children)
  }
}

const customLeaveAnimation = {
  from: { transform: 'scale(1, 1)' },
  to: { transform: 'scale(0)' }
};

/*
start
transition: transform 10000ms ease-in-out 0ms, opacity 10000ms ease-in-out 0ms;
*/


/*
out
position: absolute; top: 0px; left: 0px; right: 0px; transform: scale(0); opacity: 0; transition: transform 10000ms ease-in-out 0ms, opacity 10000ms ease-in-out 0ms;
*/
